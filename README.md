This project is simple example for handling "Out of Memory" from huge PDF Report geration by separate report into small PDF file and merge all small file into 1 PDF file.

To run this project:
1. Clone/Download the project
2. Run project via console to run with low memory to simulate Out of memory
- Open project in console
- Run "mvn clean package"
- Run "java -Xmx40m -jar ./target/huge-jasper-report-0.0.1-SNAPSHOT.jar"

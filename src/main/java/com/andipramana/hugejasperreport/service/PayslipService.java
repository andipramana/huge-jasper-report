package com.andipramana.hugejasperreport.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.andipramana.hugejasperreport.model.Payslip;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BadPdfFormatException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;

@Service
public class PayslipService {
	private final String tempFolderPath = "src/main/resources/jasper_temp"; // Change this to your actual temporary folder path
	private final String outputFolderPath = "src/main/resources/jasper_output"; // Change this to your actual temporary folder path
	private final String fileName = "mergedReport.pdf"; // Change this to your actual temporary folder path
	
	public byte[] generateReportAtOnce(Long totalPage) throws IOException, JRException {
		// Load the JasperReport template (JRXML file)
		InputStream jrxmlInput = new ClassPathResource("/jasper/my_report.jrxml").getInputStream();
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlInput);
		
		// Create data source (replace YourDataClass and fetchDataForPage with your actual data)
		List<Payslip> dataForPage = fetchDataForPage(totalPage);
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(dataForPage);
		
		// Create JasperPrint by filling the report with data
		Map<String, Object> parameters = null; // If you have report parameters, you can set them here
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
		
		byte[] reportBytes = JasperExportManager.exportReportToPdf(jasperPrint);
		return reportBytes;
	}
	
	// Simulate retrieving data for a specific range of pages
    private List<Payslip> fetchDataForPage(Long totalPage) {
        List<Payslip> data = new ArrayList<>();
        // Implement logic to retrieve data for the specified page range
        // For this example, we'll create a list of dummy data
        for (Long i = 0L; i.compareTo(totalPage) < 0; i++) {
        	Long count = i+1;
            data.add(new Payslip(count, count, "Employee " + count, "EM"+count, "Office 1", "Supervisor", "10.000.000", "200.000", "100.000", "0", "50.000", "10.250.000"));
        }
        
        return data;
    }
    
    public void generateReportWithPageLimit(Long totalPage, Long limitPage) throws IOException, JRException, DocumentException {
		deleteTempFolder();
        generatePdfReport(totalPage, limitPage);
        mergePdfFiles();
	}

	private void generatePdfReport(Long totalPage, Long limitPage)
			throws IOException, JRException {
		// Load the JasperReport template (JRXML file)
        InputStream jrxmlInput = new ClassPathResource("/jasper/my_report.jrxml").getInputStream();
        JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlInput);

        // Create a temporary folder to store PDF files
        File tempFolder = new File(tempFolderPath);
        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
        }

        // Create data source (replace YourDataClass and fetchDataForPage with your actual data)
        Long totalProcess = totalPage/limitPage;
        Long dataLeft = totalPage % limitPage;
        if(dataLeft != 0) {
        	totalProcess += 1;
        }
        
        for (Long currentPage = 1L; currentPage <= totalProcess; currentPage++) {
        	if(currentPage == totalProcess && dataLeft != 0) {
        		limitPage = dataLeft;
        	}
        	
            List<Payslip> dataForPage = fetchDataForPage(currentPage, limitPage);
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(dataForPage);

            // Create JasperPrint by filling the report with data
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource);

            // Generate individual PDF files and store them in the temporary folder
            String pdfFileName = tempFolderPath + "/page_" + getFileCount(currentPage) + ".pdf";
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
            System.out.println(pdfFileName + " save to temp folder");
        }
	}

    private String getFileCount(Long currentPage) {
		if(currentPage > 99) {
			return currentPage.toString();
		} else if(currentPage > 9) {
			return "0" + currentPage;
		} else {
			return "00" + currentPage;
		}
	}

    // Simulate retrieving data for a specific page
    private List<Payslip> fetchDataForPage(Long currentPage, Long limitPage) {
        List<Payslip> data = new ArrayList<>();
        // Implement logic to retrieve data for the specified page
        // For this example, we'll create a list of dummy data
        for (Long i = 0L; i < limitPage; i++) {
            Long count = (currentPage - 1) * limitPage + i + 1;
            data.add(new Payslip(count, count, "Employee " + count, "EM" + count, "Office 1", "Supervisor", "10.000.000", "200.000", "100.000", "0", "50.000", "10.250.000"));
        }
        
        return data;
    }
    
    private class PdfCopyWrapper {
		private PdfCopy copy;

		public PdfCopy getCopy() {
			return copy;
		}

		public void setCopy(PdfCopy copy) {
			this.copy = copy;
		}
	}

    // Merge all PDF files in the temporary folder into one PDF
    private OutputStream mergePdfFiles() throws IOException, DocumentException {
    	OutputStream mergedPdfStream = createFile();
        Document document = new Document();
        final PdfCopyWrapper copyWrapper = new PdfCopyWrapper();
        
        try {
	        copyWrapper.setCopy(new PdfCopy(document, mergedPdfStream));
			document.open();
	
			Path folderPath = Paths.get(tempFolderPath);
			Files.list(folderPath)
					.filter(Files::isRegularFile)
					.filter(path -> path.toString().toLowerCase().endsWith(".pdf"))
					.forEach(pdfFile -> {
						PdfReader reader = null;
						try {
							reader = new PdfReader(pdfFile.toString());
	
							int totalPages = reader.getNumberOfPages();
	
							System.out.println("Start merging file " + pdfFile.getFileName().toString() + " " + (reader.getFileLength()/1024) + " kb");
							for (int i = 1; i <= totalPages; i++) {
								try {
									copyWrapper.getCopy().addPage(copyWrapper.getCopy().getImportedPage(reader, i));
								} catch (BadPdfFormatException e) {
									System.out.println("Error adding page to merged PDF file: " + e.getMessage());
								}
							}
							System.out.println("Finish merging file " + pdfFile.getFileName().toString());
							System.out.println();
	
							reader.close();
						} catch (OutOfMemoryError e) {
							System.out.println("Out of memory error occurred while merging PDF file: " + pdfFile.toString());
							if (reader != null) {
								reader.close();
							}
						} catch (IOException e) {
							System.out.println("Error merging PDF file: " + e.getMessage());
							if (reader != null) {
								reader.close();
							}
						}
					});
        } finally {
        	deleteTempFolder();
			if (copyWrapper.getCopy() != null) {
				copyWrapper.getCopy().close();
			}
			if (document != null) {
				document.close();
			}
			if (mergedPdfStream != null) {
				mergedPdfStream.close();
			}
			
			System.out.println("Finish merging all file");
        }
        
        return mergedPdfStream;
    }
    
    private FileOutputStream createFile() throws IOException {
		File directory = new File(outputFolderPath);
		if (!directory.exists()) {
			directory.mkdirs();
		}

		return new FileOutputStream(outputFolderPath + File.separator + fileName);
	}

    // Delete the temporary folder and its contents
    private void deleteTempFolder() {
        File tempFolder = new File(tempFolderPath);
        File[] files = tempFolder.listFiles();

        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }

        tempFolder.delete();
    }
}

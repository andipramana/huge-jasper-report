package com.andipramana.hugejasperreport.controller;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.andipramana.hugejasperreport.service.PayslipService;
import com.lowagie.text.DocumentException;

@Controller
public class PayslipController {
	@Autowired
	private PayslipService payslipService;
	
	private final String fileNameAndPath = "src/main/resources/jasper_output/mergedReport.pdf"; // Change this to your actual temporary folder path
	private final String fileName = "mergedReport.pdf";
	
    @GetMapping("/generate-payslip")
    public void generateReport(@RequestParam Long totalPage, HttpServletResponse response) throws JRException, IOException {
    	byte[] payslipBytes = payslipService.generateReportAtOnce(totalPage);
    	response.setContentType("application/pdf");
    	response.setHeader("Content-Disposition", "attachment; filename=output.pdf");
    	response.getOutputStream().write(payslipBytes);
    }
	
    @ResponseBody
    @GetMapping("/generate-payslip-merged")
    public ResponseEntity<FileSystemResource> generateReportMerged(@RequestParam Long totalPage, @RequestParam Long limitPage) throws JRException, IOException, DocumentException {
    	payslipService.generateReportWithPageLimit(totalPage, limitPage);
        
        return downloadMergedPayslip();
    }

	private ResponseEntity<FileSystemResource> downloadMergedPayslip() {
        File file = new File(fileNameAndPath);

        // Check if the file exists
        if (file.exists()) {
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);

            // Create a FileSystemResource to wrap the file
            FileSystemResource resource = new FileSystemResource(file);

            System.out.println("Payslip created successfully at " + new Date());
            
            // Return a ResponseEntity with the file data and headers
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(file.length())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } else {
            return ResponseEntity.notFound().build();
        }
	}
	
	
}

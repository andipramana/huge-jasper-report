package com.andipramana.hugejasperreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HugeJasperReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(HugeJasperReportApplication.class, args);
	}

}

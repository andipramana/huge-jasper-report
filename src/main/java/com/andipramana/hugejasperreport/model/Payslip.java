package com.andipramana.hugejasperreport.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payslip {
    private Long payslipId;
    private Long employeeId;
    private String employeeName;
    private String employeeCode;
    private String officeName;
    private String position;
    private String basicSalary;
    private String fixAllowance;
    private String variableAllowance;
    private String deduction;
    private String tax;
    private String takeHomePay;
}
